use std::{
    collections::{BTreeSet, HashMap},
    fmt,
};

use sequoia_openpgp as openpgp;
use openpgp::policy::StandardPolicy;

use crate::{
    Data,
    Result,
    sop::{Sop, Version},
    plan::{
        Plan,
        Runnable,
        AResult,
    },
};

pub mod templates;

mod asymmetric_encryption;
mod symmetric_encryption;
mod detached_signatures;
mod inline_signatures;
mod hashes;
mod compression;
mod key_generation;
mod certificates;
mod messages;
mod ecc;
mod packet_parser;
mod armor;

/// Message used in tests.
///
/// For consistency, all tests that sign/encrypt a message should use
/// this statement.
pub const MESSAGE: &[u8] = b"Hello World :)";

/// Password used in tests.
///
/// For consistency, all tests that encrypt a message should use this
/// password.
pub const PASSWORD: &str = "password";

/// A grocery list that is tricky.
///
/// The "From "-prefixed line SHOULD and the "- "-prefixed lines MUST
/// be dash-escaped.  Finally, this list ends in two newlines.
pub const TRICKY_GROCERY_LIST: &[u8] = b"From the grocery store we need:

- tofu
- vegetables
- noodles

";

/// A StandardPolicy for the tests to use.
const P: &StandardPolicy = &StandardPolicy::new();

/// Metadata for the tests.
pub trait Test: Runnable<TestMatrix> {
}

/// States the expected result of a test.
type Expectation = std::result::Result<String, String>;

/// Errors returned by check_producer or check_consumer.
#[derive(thiserror::Error, Debug)]
pub enum CheckError {
    /// Hard Errors, indicating that the output does not conform to the
    /// SOP specification. This causes the test score to be Unknown.
    #[error("{0}")]
    HardFailure(String),
    /// Soft Errors, indicating that the output may not conform to the
    /// OpenPGP specification, or may conform in a different way than
    /// requested. This causes the test score to be Failure, Neutral or
    /// Success, depending on the test expectation.
    #[error("{0}")]
    SoftFailure(String),
}

/// Checks that artifacts can be used by all implementations.
pub trait ConsumptionTest<C: AsData, R: AsData>: Runnable<TestMatrix> {
    fn produce(&self) -> Result<Vec<(String, C, Option<Expectation>)>>;
    fn consume(&self, pgp: &Sop, artifact: &C)
               -> Result<R>;
    fn check_consumer(&self, _artifact: &C, _result: &R,
                      _expectation: &Option<Expectation>)
                      -> Result<()> {
        Ok(())
    }
    fn run(&self, implementations: &[crate::Sop]) -> Result<TestMatrix>
    {
        let mut test_results = Vec::new();

        for (description, produced, expectation)
            in self.produce()?.into_iter()
        {
            let class = Class::new(&description, &expectation);
            let artifact = Artifact::ok(description, produced.as_data().into());

            let mut results = Vec::new();
            for c in implementations.iter() {
                let result = self.consume(c, &produced);
                let mut a = match result {
                    Ok(r) => {
                        if let Err(e) = self.check_consumer(&produced, &r,
                                                            &expectation) {
                            Artifact::err(c.version()?.to_string(),
                                      r.as_data().into(), &e)
                        } else {
                            Artifact::ok(c.version()?.to_string(),
                                         r.as_data().into())
                        }
                    },
                    Err(e) =>
                        Artifact::err(c.version()?.to_string(),
                                      Default::default(), &e),
                };

                a.set_score(&expectation);
                results.push(a);
            }

            test_results.push(TestResults {
                artifact: artifact.limit_data_size(),
                class,
                results,
                expectation,
            });
        }

        Ok(TestMatrix {
            title: self.title(),
            slug: crate::templates::slug(&self.title()),
            tags: self.tags().into_iter().map(Into::into).collect(),
            description: maybe_add_paragraph(self.description()),
            artifacts: self.artifacts(),
            consumers: implementations.iter().map(|i| i.version().unwrap())
                .collect(),
            results: test_results,
        })
    }
}

/// Checks that artifacts produced by one implementation can be used
/// by another.
pub trait RoundtripTest<C: AsData, R: AsData>: Runnable<TestMatrix> {
    fn produce(&self, pgp: &Sop) -> Result<C>;
    fn produce_artifacts(&self, pgp: &Sop)
                         -> Result<Vec<(Option<String>, Result<C>)>>
    {
        Ok(vec![(None, self.produce(pgp))])
    }

    fn check_producer(&self, _artifact: &C) -> Result<()> {
        Ok(())
    }
    fn consume(&self,
               producer: &Sop,
               consumer: &Sop,
               artifact: &C) -> Result<R>;
    fn check_consumer(&self, _artifact: &C, _result: &R) -> Result<()> {
        Ok(())
    }
    fn expectation(&self, _artifact: Option<&C>) -> Option<Expectation> {
        Some(Ok("Interoperability concern.".into()))
    }
    fn run(&self, implementations: &[crate::Sop]) -> Result<TestMatrix>
    {
        let mut test_results = Vec::new();

        for p in implementations.iter() {
          for (variant, consumable) in self.produce_artifacts(p)? {
            let label = if let Some(v) = variant {
                format!("{}, {}", p.version()?, v)
            } else {
                p.version()?.to_string()
            };

            let (mut artifact, expectation) = match &consumable {
                Ok(r) => {
                    let a = if let Err(e) = self.check_producer(&r) {
                        Artifact::err(label,
                                      r.as_data().into(), &e)
                    } else {
                        Artifact::ok(label, r.as_data().into())
                    };
                    (a, self.expectation(Some(&r)))
                },
                Err(e) => {
                    let a = Artifact::err(label,
                                          Default::default(), &e);
                    (a, self.expectation(None))
                },
            };
            let class = Class::new("", &expectation);
            artifact.set_score(&expectation);

            let mut results = Vec::new();
            if artifact.error.is_empty() {
                let consumable = consumable.expect("errors checked above");

                for c in implementations.iter() {
                    let result = self.consume(p, c, &consumable);
                    let mut a = match result {
                        Ok(r) => {
                            if let Err(e) = self.check_consumer(&consumable,
                                                                &r) {
                                Artifact::err(c.version()?.to_string(),
                                              r.as_data().into(), &e)
                            } else {
                                Artifact::ok(c.version()?.to_string(),
                                             r.as_data().into())
                            }
                        },
                        Err(e) =>
                            Artifact::err(c.version()?.to_string(),
                                          Default::default(), &e),
                    };

                    a.set_score(&expectation);
                    results.push(a);
                }
            }

            test_results.push(TestResults {
                artifact: artifact.limit_data_size(),
                class,
                results,
                expectation,
            });
          }
        }

        Ok(TestMatrix {
            title: self.title(),
            slug: crate::templates::slug(&self.title()),
            tags: self.tags().into_iter().map(Into::into).collect(),
            description: maybe_add_paragraph(self.description()),
            artifacts: self.artifacts(),
            consumers: implementations.iter().map(|i| i.version().unwrap())
                .collect(),
            results: test_results,
        })
    }
}

/// Given an produced artifact with additional context, returns the
/// raw artifact data as it is consumed or produced by the SOP
/// implementations.
pub trait AsData {
    fn as_data(&self) -> &[u8];
}

impl AsData for Data {
    fn as_data(&self) -> &[u8] {
        self
    }
}

impl<T0> AsData for (Data, T0) {
    fn as_data(&self) -> &[u8] {
        &self.0
    }
}

impl<T0, T1> AsData for (Data, T0, T1) {
    fn as_data(&self) -> &[u8] {
        &self.0
    }
}

impl<T0, T1, T2> AsData for (Data, T0, T1, T2) {
    fn as_data(&self) -> &[u8] {
        &self.0
    }
}

impl AsData for () {
    fn as_data(&self) -> &[u8] {
        &[]
    }
}

impl<T0> AsData for ((), T0) {
    fn as_data(&self) -> &[u8] {
        &[]
    }
}

impl<T0, T1> AsData for ((), T0, T1) {
    fn as_data(&self) -> &[u8] {
        &[]
    }
}

impl<T0, T1, T2> AsData for ((), T0, T1, T2) {
    fn as_data(&self) -> &[u8] {
        &[]
    }
}

impl AsData for Vec<crate::sop::Verification> {
    fn as_data(&self) -> &[u8] {
        &[]
    }
}

/// Artifacts produced by producers.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct Artifact {
    producer: String,
    data: Data,
    error: String,
    score: Score,
}

/// A score associated with an artifact.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
enum Score {
    Neutral,
    Success,
    Failure,
    Unsupported,
    IO,
    Unknown,
}

impl Score {
    fn hard_failure(e: &anyhow::Error) -> Option<Score> {
        if let Some(e) = e.downcast_ref::<crate::sop::ErrorWithOutput>() {
            use crate::sop::SOPError::*;
            match e.source {
                UnsupportedOption
                    | UnsupportedSubcommand
                    | UnsupportedSpecialPrefix => Some(Score::Unsupported),
                IoError(_) => Some(Score::IO),
                _ => None,
            }
        } else if let Some(_) = e.downcast_ref::<std::io::Error>() {
            Some(Score::IO)
        } else if let Some(e) = e.downcast_ref::<CheckError>() {
            use CheckError::*;
            match e {
                HardFailure(_) => Some(Score::Unknown),
                SoftFailure(_) => None,
            }
        } else {
            Some(Score::Unknown)
        }
    }
}

impl From<anyhow::Error> for Score {
    fn from(e: anyhow::Error) -> Score {
        Score::from(&e)
    }
}

impl From<&anyhow::Error> for Score {
    fn from(e: &anyhow::Error) -> Score {
        if let Some(e) = e.downcast_ref::<crate::sop::ErrorWithOutput>() {
            use crate::sop::SOPError::*;
            match e.source {
                UnsupportedOption
                    | UnsupportedSubcommand
                    | UnsupportedSpecialPrefix => Score::Unsupported,
                IoError(_) => Score::IO,
                _ => Score::Failure,
            }
        } else if let Some(_) = e.downcast_ref::<std::io::Error>() {
            Score::IO
        } else {
            Score::Unknown
        }
    }
}

impl Artifact {
    fn ok(producer: String, data: Data) -> Self {
        Self {
            producer,
            data,
            error: Default::default(),
            score: Score::Neutral,
        }
    }

    fn err(producer: String, data: Data, err: &anyhow::Error) -> Self {
        use std::fmt::Write;
        let mut error = String::new();
        writeln!(error, "{}", err).unwrap();
        err.chain().skip(1)
            .for_each(|cause| writeln!(error, "  because: {}", cause).unwrap());

        Self {
            producer,
            data,
            error,
            score: Score::hard_failure(err).unwrap_or(Score::Neutral),
        }
    }

    fn set_score(&mut self, expectation: &Option<Expectation>) {
        if let (Score::Neutral, Some(e)) = (&self.score, expectation) {
            self.score = if e.is_ok() == self.error.is_empty() {
                Score::Success
            } else {
                Score::Failure
            }
        }
    }

    /// Limits the artifact size.
    ///
    /// In order not to bloat the report too much, we limit the size
    /// of artifacts included in the report.  If the data exceeds the
    /// configured size, it is dropped.
    fn limit_data_size(mut self) -> Self {
        if self.data.len() > crate::MAXIMUM_ARTIFACT_SIZE {
            self.data = Default::default();
        }
        self
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct TestMatrix {
    title: String,
    slug: String,
    tags: BTreeSet<String>,
    description: String,
    artifacts: Vec<(String, Data)>,
    consumers: Vec<Version>,
    results: Vec<TestResults>,
}

impl AResult for TestMatrix {
    fn with_title(mut self, title: String) -> Self {
        self.title = title;
        self
    }

    fn with_description(mut self, description: String) -> Self {
        self.description = description;
        self
    }

    fn with_artifacts(mut self, artifacts: Vec<(String, crate::Data)>) -> Self {
        self.artifacts = artifacts;
        self
    }

    fn with_tags(mut self, tags: BTreeSet<&'static str>) -> Self {
        self.tags = tags.into_iter().map(Into::into).collect();
        self
    }
}

impl TestMatrix {
    pub fn title(&self) -> String {
        self.title.clone()
    }

    pub fn summarize(&self, summary: &mut Summary) {
        for (i, imp) in self.consumers.iter().enumerate() {
            let mut results = 0;
            let mut successes = 0;
            let mut failures = 0;
            let mut errors = 0;
            let mut matched_expectations = true;

            let mut handle_result = |score: &Score, expectation: &Option<Expectation>| {
                results += 1;

                use Score::*;
                match score {
                    Neutral => (),
                    Success => successes += 1,
                    Failure => failures += 1,
                    Unsupported | IO | Unknown => errors += 1,
                }

                matched_expectations &= match (score, expectation) {
                    (_, None) =>
                        true, // Vacuous truth, handled below.
                    (Success, Some(_)) =>
                        true, // The expectation has been checked already.
                    (Failure, Some(_)) =>
                        false, // The expectation has not been met.
                    _ => false,
                };
            };

            for row in &self.results {
                // If this row's artifact was produced by
                // implementation 'imp', count the artifact score.
                if row.artifact.producer == imp.summary {
                    handle_result(&row.artifact.score, &row.expectation);
                }

                // Get the result corresponding to implementation
                // 'imp'.
                if let Some(r) = row.results.get(i) {
                    handle_result(&r.score, &row.expectation);
                }
            }

            // If there are no results from this implementation, don't
            // count the expectations as being matched.
            if results == 0 {
                matched_expectations = false;
            }

            // If there are no expectations, omit this test from the
            // calculations.
            let mut test_result = Some(matched_expectations);
            if ! self.results.iter().any(|row| row.expectation.is_some()) {
                test_result = None;
            }

            summary.add(imp.clone(), successes, failures, errors, test_result);
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[derive(serde::Deserialize, serde::Serialize)]
enum Class {
    Default,
    BaseCase,
}

impl Class {
    fn new(description: &str, expectation: &Option<Expectation>) -> Self {
        if description.to_lowercase().contains("base case")
            || match expectation {
                Some(Ok(s)) | Some(Err(s)) =>
                    s.to_string().to_lowercase().contains("base case"),
                None => false,
            }
        {
            Class::BaseCase
        } else {
            Class::Default
        }
    }
}


#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct TestResults {
    artifact: Artifact,
    class: Class,
    results: Vec<Artifact>,
    expectation: Option<Expectation>,
}

#[derive(Debug, Default, serde::Serialize)]
pub struct Summary {
    score: HashMap<Version, Scores>,
}

impl Summary {
    fn add(&mut self, imp: Version,
           successes: usize, failures: usize, errors: usize,
           test_result: Option<bool>) {
        let e = self.score.entry(imp).or_default();
        e.vector_good += successes;
        e.vector_bad += failures + errors;
        match test_result {
            None => (),
            Some(true) => e.test_good += 1,
            Some(false) => e.test_bad += 1,
        }
    }

    /// Transforms the summary into a map suitable for rendering.
    pub fn for_rendering(self) -> Vec<(String, Scores)> {
        let mut r: Vec<(String, Scores)> =
            self.score.into_iter()
            .map(|(k, v)| (k.to_string(), v))
            .collect();
        r.sort_unstable_by(|a, b| a.1.cmp(&b.1).reverse());
        r
    }
}

#[derive(Debug, Default, PartialEq, Eq, serde::Serialize)]
pub struct Scores {
    vector_good: usize,
    vector_bad: usize,
    test_good: usize,
    test_bad: usize,
}

impl Ord for Scores {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.test_good.cmp(&other.test_good)
            .then(self.vector_good.cmp(&other.vector_good))
    }
}

impl PartialOrd for Scores {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

/// Extracts the public certificate from the given key.
pub fn extract_cert(key: &[u8]) -> Result<Data> {
    use openpgp::Packet;
    use openpgp::parse::{Parse, PacketParser, PacketParserResult};
    use openpgp::serialize::{Serialize, stream::*};
    let mut cert = Vec::new();
    let sink = Message::new(&mut cert);
    let mut sink = Armorer::new(sink)
        .kind(openpgp::armor::Kind::PublicKey)
        .build()?;

    let mut ppr = PacketParser::from_bytes(key)?;
    while let PacketParserResult::Some(pp) = ppr {
        let (packet, ppr_) = pp.next()?;
        ppr = ppr_;
        match packet {
            Packet::SecretKey(k) =>
                Packet::PublicKey(k.parts_into_public())
                    .serialize(&mut sink)?,
            Packet::SecretSubkey(k) =>
                Packet::PublicSubkey(k.parts_into_public())
                    .serialize(&mut sink)?,
            p => p.serialize(&mut sink)?,
        }
    }
    sink.finalize()?;

    Ok(cert.into())
}

pub type TestPlan<'a> = Plan<'a, TestMatrix>;

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    asymmetric_encryption::schedule(plan)?;
    symmetric_encryption::schedule(plan)?;
    detached_signatures::schedule(plan)?;
    inline_signatures::schedule(plan)?;
    hashes::schedule(plan)?;
    compression::schedule(plan)?;
    key_generation::schedule(plan)?;
    certificates::schedule(plan)?;
    messages::schedule(plan)?;
    armor::schedule(plan)?;
    ecc::schedule(plan)?;
    packet_parser::schedule(plan)?;
    Ok(())
}

/// Turns a sequence of packets into an armored data stream.
pub fn make_test<T, I, P>(test: T, packets: I,
                          label: openpgp::armor::Kind,
                          expectation: Option<Expectation>)
                          -> Result<(String, Data, Option<Expectation>)>
where T: AsRef<str>,
      I: IntoIterator<Item = P>,
      P: std::borrow::Borrow<openpgp::Packet>,
{
    use openpgp::serialize::Serialize;

    let mut buf = Vec::new();
    {
        use openpgp::armor;
        let mut w =
            armor::Writer::new(&mut buf, label)?;
        for p in packets {
            p.borrow().serialize(&mut w)?;
        }
        w.finalize()?;
    }
    Ok((test.as_ref().into(), buf.into(), expectation))
}

/// Adds a paragraph tag if there is none in the description.
fn maybe_add_paragraph(description: String) -> String {
    if description.trim_start().starts_with('<') {
        description
    } else {
        format!("<p>{}</p>", description)
    }
}

/// Tests if two messages are equal after CSF normalization.
pub fn csf_eq(a: &[u8], b: &[u8]) -> bool {
    // Strategy: normalize both messages, then compare.
    let a_ = csf_normalize(a);
    let b_ = csf_normalize(b);
    match (a_, b_) {
        (Ok(a), Ok(b)) => a == b,
        _ => false,
    }
}

/// Normalizes a message like the CSF transformation.
pub fn csf_normalize(m: &[u8]) -> Result<Vec<u8>> {
    Ok(std::str::from_utf8(m)?
        .split('\n')
        .map(|line| line.trim_end_matches(|c| c == ' ' || c == '\t' || c == '\r'))
        .collect::<Vec<&str>>()
        .join("\n")
        .into())
}

/// Formats a list of things, like algorithms, nicely.
pub struct ListOf<'l, A: fmt::Display>(&'l [A]);
impl<'l, A: fmt::Display> fmt::Display for ListOf<'l, A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.0.len() != 1 {
            f.write_str("[")?;
        }
        for (i, a) in self.0.iter().enumerate() {
            if i > 1 {
                f.write_str(", ")?;
            }
            write!(f, "{}", a)?;
        }
        if self.0.len() != 1 {
            f.write_str("]")?;
        }
        Ok(())
    }
}

