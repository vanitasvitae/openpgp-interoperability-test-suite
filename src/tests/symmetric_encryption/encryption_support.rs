use anyhow::{bail, ensure, Context};

use sequoia_openpgp::{
    self as openpgp,
    cert::amalgamation::ValidAmalgamation,
    parse::Parse,
    serialize::SerializeInto,
    types::{
        AEADAlgorithm,
        Features,
        KeyFlags,
        SymmetricAlgorithm,
        Timestamp,
    },
};

use crate::{
    Data,
    Result,
    data,
    sop::Sop,
    tests::{
        CheckError,
        ConsumptionTest,
        Expectation,
        ListOf,
        MESSAGE,
        TestMatrix,
    },
};


/// Tests support for the production (i.e. encryption) of symmetric
/// encryption algorithms.
pub struct SymmetricEncryptionSupport {
    aead: Option<AEADAlgorithm>,
    additional_certs: Vec<Data>,
}

impl SymmetricEncryptionSupport {
    pub fn new() -> Result<SymmetricEncryptionSupport> {
        Ok(SymmetricEncryptionSupport {
            aead: None,
            additional_certs: Vec::new(),
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for SymmetricEncryptionSupport {
    fn title(&self) -> String {
        "Symmetric Encryption Algorithm support, production side".into()
    }

    fn description(&self) -> String {
        "This tests support for the production (i.e. encryption) of \
         different symmetric encryption algorithms.  To that end, \
         the symmetric algorithm preferences are changed to only include \
         the algorithm to be tested.".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![("Key".into(), data::certificate("bob-secret.pgp").into())]
    }

    fn run(&self, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, implementations)
    }
}

impl ConsumptionTest<(Data, SymmetricAlgorithm, Option<AEADAlgorithm>), Data>
    for SymmetricEncryptionSupport
{
    fn produce(&self)
               -> Result<Vec<(String,
                              (Data, SymmetricAlgorithm, Option<AEADAlgorithm>),
                              Option<Expectation>)>>
    {
        use SymmetricAlgorithm::*;

        let cert =
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
        let mut t = Vec::new();

        for cipher in SymmetricAlgorithm::variants()
            .chain(std::iter::once(SymmetricAlgorithm::Unencrypted))
        {
            // Change the cipher preferences of CERT.
            let uid = cert.with_policy(crate::tests::P, None).unwrap()
                .primary_userid().unwrap();
            let mut builder = openpgp::packet::signature::SignatureBuilder::from(
                uid.binding_signature().clone())
                .set_signature_creation_time(Timestamp::now())?
                .set_preferred_symmetric_algorithms(vec![cipher])?;
            if let Some(algo) = self.aead {
                builder = builder.set_preferred_aead_algorithms(vec![algo])?;
                builder = builder.set_features(
                    Features::empty().set_seipdv1().set_aead())?;
            }
            let mut primary_keypair =
                cert.primary_key()
                .key().clone().parts_into_secret()?.into_keypair()?;
            let new_sig = uid.bind(&mut primary_keypair, &cert, builder)?;
            let cert = cert.clone().insert_packets(Some(new_sig))?;
            let cert = cert.armored().to_vec()?;

            let expectation = match cipher {
                IDEA | TripleDES | CAST5 => // Deprecated in the crypto refresh.
                    Some(Err("Algorithm should be avoided.".into())),
                AES128 =>
                    Some(Ok("Implementations MUST implement AES-128.".into())),
                AES256 =>
                    Some(Ok("Implementations SHOULD implement AES-256.".into())),
                Unencrypted =>
                    Some(Err("\"Unencrypted cipher\" MUST NOT be used.".into())),
                _ => None,
            };

            t.push((
                format!("{:?}", cipher),
                (cert.into(), cipher, None),
                expectation,
            ));
        }

        Ok(t)
    }

    fn consume(&self,
               pgp: &Sop,
               (cert, _cipher, _aead):
                   &(Data, SymmetricAlgorithm, Option<AEADAlgorithm>))
               -> Result<Data>
    {
        pgp.sop()
            .encrypt()
            .cert(cert)
            .plaintext(MESSAGE)
    }

    fn check_consumer(&self,
                      (_cert, cipher, aead):
                          &(Data, SymmetricAlgorithm, Option<AEADAlgorithm>),
                      ciphertext: &Data,
                      _expectation: &Option<Expectation>)
                      -> Result<()>
    {
        let pp = openpgp::PacketPile::from_bytes(&ciphertext)
            .context("Produced data is malformed")?;

        // Check that the number of PKESK packets matches the number of
        // certificates.
        let pkesk_count = pp.children()
            .filter(|p| p.tag() == openpgp::packet::Tag::PKESK)
            .count();
        ensure!(pkesk_count == 1 + self.additional_certs.len(),
            CheckError::HardFailure(format!(
                "Expected {:?} PKESK packets, found {:?}",
                1 + self.additional_certs.len(), pkesk_count)));

        // Check whether the implementation used the algorithm(s) that
        // were requested, if any. Note that using a different algorithm
        // is only a "soft failure", as the artifact may still be valid
        // (but using a different algorithm), and the test expectations
        // indicate whether the algorithm is expected to be implemented.

        if let Some(aead_algo) = aead {
            match pp.children().last() {
                Some(openpgp::Packet::AED(a)) => {
                    ensure!(a.aead() == *aead_algo,
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}, but {:?}",
                            aead_algo, a.aead())));

                    ensure!(a.symmetric_algo() == *cipher,
                            CheckError::SoftFailure(format!(
                                "Producer did not use {:?} but {:?}",
                                cipher, a.symmetric_algo())));
                },
                Some(p) => bail!(CheckError::SoftFailure(format!(
                    "Producer did not use AEAD, found {} packet", p.tag()))),
                None => bail!(CheckError::HardFailure("No packet emitted".into())),
            }
        } else {
            // Check that the producer used CIPHER.
            let cert =
                openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?;
            let mode = KeyFlags::empty()
                .set_storage_encryption().set_transport_encryption();

            let mut ok = false;
            let mut algos = Vec::new();
            'search: for p in pp.children() {
                if let openpgp::Packet::PKESK(p) = p {
                    for ka in cert.keys().with_policy(crate::tests::P, None)
                        .secret()
                        .key_flags(mode.clone())
                    {
                        let mut keypair = ka.key().clone().into_keypair()?;
                        if let Some((a, _)) = p.decrypt(&mut keypair, None) {
                            if a == *cipher {
                                ok = true;
                                break 'search;
                            }
                            algos.push(a);
                        }
                    }
                }
            }

            ensure!(ok,
                CheckError::SoftFailure(format!(
                    "Producer did not use {:?}, but {}", cipher, ListOf(&algos))));
        }

        Ok(())
    }

}
